import requests
import pandas as pd
import io
import torch
from torchvision.transforms import ToTensor
import torch.utils.data as td

class data(td.Dataset):

    def __init__(self,path,delimiter,header=0,transform=None):
        self.data = pd.read_csv(path, delimiter=delimiter,header=header,skipinitialspace=True)
        self.transform = transform
    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        value = self.data.iloc[index, :].values

        if self.transform is not None:
            value = self.transform(value)

        return value



url = 'https://github.com/WinVector/zmPDSwR/blob/master/UCICar/car.data.csv'
# car = pd.read_csv('/home/hariactyv/TEST_R/PDSwR2-master/UCICar/car.data.csv', delimiter='\s',
#                   header=0,
#                   skipinitialspace=True)

car = data('/home/hariactyv/Test_R/PDSwR2-master/UCICar/car.data.csv', delimiter=' ')
print(len(car))
print(car.__getitem__(1))
# print(car.columns)
# print(car.shape)
# print(car.dtypes)
# print(type(car))
# print(car.describe())
# print(car.info())