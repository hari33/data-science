import pandas as pd
#df = pd.read_csv('/home/hari/machine_learning/PDS_master/data/ss11pusa.csv')
#df.columns = [c.lower() for c in df.columns] #postgres doesn't like capitals or spaces

from sqlalchemy import create_engine
engine = create_engine('postgresql://hari:hari@localhost:5432/hari')

#df.to_sql("ss11pusa", engine) # converting Data Frame to Table in Data base
dhus = pd.read_sql_query('SELECT * FROM hus LIMIT 1000',con=engine)
print(dhus.head(10))
print(dhus.shape)
dhus.to_csv('../data/dhus.csv', index=False)

dpus =pd.read_sql_query('SELECT pus.* FROM pus WHERE pus.serialno IN '
                        '(SELECT DISTINCT hus.serialno FROM hus LIMIT 1000)', con=engine)
print(dpus.head(10))
print(dpus.shape)
dpus.to_csv('../data/dpus.csv', index=False)

# creating a subset
## converting data frame columns to numeric because pgfutter
# uploaded the csv as text columns
psub =  dpus[(pd.to_numeric(dpus.pincp) > 1000) & (pd.to_numeric(dpus.esr)==1) &
             (pd.to_numeric(dpus.pincp) <= 250000) & (pd.to_numeric(dpus.pernp) >1000)
             & (pd.to_numeric(dpus.pernp) <= 250000)  & (pd.to_numeric(dpus.wkhp) >= 40)
             & (pd.to_numeric(dpus.agep) >= 20) & (pd.to_numeric(dpus.agep) <= 50)
             & (pd.to_numeric(dpus.pwgtp1) > 0) & (pd.to_numeric(dpus.cow).isin(range(1,7)))
             & (pd.to_numeric(dpus.schl).isin(range(1,24)))]
print(psub)
psub.to_csv('../data/psub.csv', index=False)

sex_map ={ '1' : 'M', '2' : 'F'}
psub['sex'] = psub['sex'].map(sex_map)

# MultiIndex to implemeted
print(psub['sex'])
print(type(psub))
print(psub.shape)
cow_map = { '1' : "Employee of a private for-profit",
            '2' : "Private not-for-profit employee",
            '3' : "Local government employee",
            '4' : "State government employee",
            '5' : "Federal government employee",
            '6' : "Self-employed not incorporated",
            '7' : "Self-employed incorporated" }
psub['cow'] = psub['cow'].map(cow_map)
print(psub['cow'])
# MultiIndex to implemeted


schl_map = {('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15') :
               "no high school diploma",
           '16' : "Regular high school diploma",
           '17' : "GED or alternative credential",
           '18' : "some college credit, no degree",
           '19' : "some college credit, no degree",
           '20' : "Associate's degree",
           '21' : "Bachelor's degree",
           '22' : "Master's degree",
           '23' : "Professional degree",
           '24' : "Doctorate degree"}
psub['schl'] = psub['schl'].map(schl_map)
print(psub['schl'])
# MultiIndex to implemeted
psub.set_index(['sex','cow','schl'], inplace=True)
psub.sort_index(inplace=True)
print(psub)


from sklearn.model_selection import train_test_split

dtrain, dtest = train_test_split(psub, test_size=0.5, random_state=42)
print(dtrain.shape)
print(dtest.shape)
#print(dtrain['cow'].describe())


