import requests
import pandas as pd
import io
from collections import Counter

# reading a tab separated file withoud column names returns Pandas Data Frame
data = pd.read_csv('/home/hariactyv/Test_R/PDSwR2-master/Statlog/german.data', delimiter=' ',
                  header=None,
                  skipinitialspace=True)

## assignning  names to columns of Data Frame
data.columns = ['Status_of_existing_checking_account', 'Duration_in_month',
                'Credit_history', 'Purpose', 'Credit_amount', 'Savings_account_bonds',
                'Present_employment_since',
                'Installment_rate_in_percentage_of_disposable_income',
                'Personal_status_and_sex', 'Other_debtors_guarantors',
                'Present_residence_since', 'Property', 'Age_in_years',
                'Other_installment_plans', 'Housing',
                'Number_of_existing_credits_at_this_bank', 'Job',
                'Number_of_people_being_liable_to_provide_maintenance_for',
                'Telephone', 'foreign_worker', 'Good_Loan']

## creating a map of codes to values using python dictionary
mapping = { 'A11' : '... < 0 DM', 'A12' : '0 < =  ... < 200 DM',
            'A13' : '... > =  200 DM / salary assignments for at least 1 year',
            'A14' : 'no checking account',
            'A30' : 'no credits taken/all credits paid back duly',
            'A31' : 'all credits at this bank paid back duly',
            'A32' : 'existing credits paid back duly till now',
            'A33' : 'delay in paying off in the past',
            'A34' : 'critical account/other credits existing (not at this bank)',
            'A40' : 'car (new)', 'A41' : 'car (used)', 'A42' : 'furniture/equipment',
            'A43' : 'radio/television', 'A44' : 'domestic appliances',
            'A45' : 'repairs', 'A46' : 'education',
            'A47' : '(vacation - does not exist?)','A48' : 'retraining',
            'A49' : 'business', 'A410' : 'others', 'A61' : '... < 100 DM',
            'A62' : '100 < =  ... < 500 DM', 'A63' : '500 < =  ... < 1000 DM',
            'A64' : '.. > =  1000 DM', 'A65' : 'unknown/ no savings account',
            'A71' : 'unemployed', 'A72' : '... < 1 year', 'A73' : '1 < =  ... < 4 years',
            'A74' : '4 < =  ... < 7 years', 'A75' : '.. > =  7 years',
            'A91' : 'male : divorced/separated', 'A92' : 'female : divorced/separated/married',
            'A93' : 'male : single', 'A94' : 'male : married/widowed', 'A95' : 'female : single',
            'A101' : 'none', 'A102' : 'co-applicant', 'A103' : 'guarantor',
            'A121' : 'real estate', 'A122' : 'if not A121 : building society savings agreement/life insurance',
            'A123' : 'if not A121/A122 : car or other, not in attribute 6',
            'A124' : 'unknown / no property', 'A141' : 'bank', 'A142' : 'stores', 'A143' : 'none',
            'A151' : 'rent', 'A152' : 'own', 'A153' : 'for free',
            'A171' : 'unemployed/ skilled - non-resident',
            'A172' : 'unskilled - resident',
            'A173' : 'skilled employee / official',
            'A174' : 'management/ self-employed/highly qualified employee/ officer',
            'A191' : 'none',
            'A192' : 'yes, registered under the customers name',
            'A201' :  'yes',
            'A202' :  'no'}

# selecting columns on type of data as a list
names  =  data.select_dtypes(include=['object']).columns.tolist()

# mapping codes in columns to valuses using map function of python
for name in names:
    data[name] = data[name].map(mapping)

print('-x-' * 50)
print(names)
print('-x-' * 50)

# view the first n rows of Pandas Data Frame
print(data.head(10))
print('-x-' * 50)

# view names Of columns of Pandas Data Frame
print(data.columns)
print('-x-' * 50)

# view the number of row and columns of the Pandas Data Frame
# returns a tuple the first dimension is the number of row and the second dimension is
# the number of columns
print(data.shape)
print('-x-' * 50)

# view the data type of each column of the Pandas Data Frame
print(data.dtypes)
print('-x-' * 50)

# view the object type of the Pandas Data Frame
print(type(data))
print('-x-' * 50)

# View the statistical summary of all columns of Pandas Data Frame
print(data.describe(include='all'))
print('-x-' * 50)

# Basic data type, counts and missing values summary of columns of Data Frame
print(data.info(null_counts=True, verbose=True))
print('-x-' * 50)

## condition for tabling
# data = data[data['Good_Loan']==1]
# print(data.head(10))
#

## viewing the each column values and count of values of pandas data frame
#First method of tabling
for name in data.columns:
    print(name.upper())
    x = data[name]
    counts = x.value_counts()
    print(counts)


cols = data.columns.tolist()
cols = cols[:-1]
df2 = (data.melt(id_vars='Good_Loan', value_vars=cols)
       .groupby([pd.Grouper(key='Good_Loan'),'variable', 'value'])
       .size()
       .unstack(level=[1,2], fill_value=0))
print (df2)

##  cross tabulation of Purpose against Good_Loan  in python using pandas library
# equivalent to r table command
cols = ['Purpose']
df2 = (data.melt(id_vars='Good_Loan', value_vars=cols)
       .groupby([pd.Grouper(key='Good_Loan'),'variable', 'value'])
       .size()
       .unstack(level=[1,0], fill_value=0))
print(df2)
print('-x-' * 50)

# cols = []
# df2 = (data.melt(id_vars='Good_Loan', value_vars=cols)
#        .groupby([pd.Grouper(key='Good_Loan'),'variable', 'value'])
#        .size()
#        .unstack(level=[1,0], fill_value=0))
# print(df2)
# print('-x-' * 50)