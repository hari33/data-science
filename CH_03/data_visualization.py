import pandas as pd
import numpy as np
import pandas_profiling
import matplotlib.pyplot as plt
import seaborn as sns

# reading tab separated file from disk
custdata = pd.read_csv('/home/hariactyv/Machine_Learning/PDS_master/data/custdata.tsv',
                       delimiter='\t')

## data mining using pandas_profiling
## creates complete summary report of data frame as html report
## which can be viewed in a browser
profile = pandas_profiling.ProfileReport(custdata)
profile.to_file(output_file='./report.html')

print(custdata.columns)
print(custdata.describe(include='all').transpose)
print(custdata.info(null_counts=True, verbose=True))


# Histogram of age of custdata matplotlib
plt.hist(custdata['age'], color = 'blue', edgecolor = 'black',
         bins = 5)
plt.title('Histogram of Age')
plt.xlabel('age')
plt.ylabel('count')
plt.show()

# densityplot of age of custdata matplotlib
plt.hist(custdata['age'],density=True)
plt.show()

# seaborn histogram
sns.distplot(custdata['age'], hist=True, kde=False,
             bins=5, color = 'blue',
             hist_kws={'edgecolor':'black'})
# Add labels
plt.title('Histogram of Age')
plt.xlabel('age')
plt.ylabel('count')
plt.show()

# Density Plot and Histogram of age
sns.distplot(custdata['age'], hist=True, kde=True,
             bins=5, color = 'darkblue',
             hist_kws={'edgecolor':'black'},
             kde_kws={'linewidth': 4})
plt.show()

# Density Plot income and log scale
sns.distplot(custdata['income'], hist=False, kde=True,
                 kde_kws={'linewidth': 4})

plt.title('Density Plot of Income')
plt.xlabel('Income')
plt.ylabel('Density')
plt.show()

g = sns.distplot(np.log10(custdata['income']), hist=False, kde=True,
                 kde_kws={'linewidth': 4})
plt.xticks(np.arange(8), ('$0','$100', '$1000', '$10,000', '$100,000', '$1,000,000', '10,000,000'))
plt.title('Density Plot of log scale of Income')
plt.xlabel('Income')
plt.ylabel('Density')
plt.show()

# Bar chart of matrial.stat

plt.bar(custdata['marital.stat'].value_counts().index, 
        custdata['marital.stat'].value_counts().values, align='center', alpha=0.5)
plt.show()

sns.barplot(x=custdata['marital.stat'].value_counts().index,
              y=custdata['marital.stat'].value_counts().values)
plt.xlabel('Marital Status')
plt.ylabel('Frequency')
plt.title('Marital Status Bar Plot')
plt.show()

# horizondal bar plot of state.of.res

plt.barh(custdata['state.of.res'].value_counts().index,
         custdata['state.of.res'].value_counts().values, align='center', alpha=0.5, color='g')
plt.tick_params(axis="y", labelsize=7)
plt.xlabel('State of Rescidency')        
plt.xlabel('Frequency')
plt.title('State of Rescidency Horizontal Bar Plot')
plt.show()

## Analysis of data using different plots using plotnine library similar to ggplot of R
from plotnine import *

# histogrom plot of age of
fig = (
    ggplot(custdata.dropna(subset = ['age'])) +
    geom_histogram(aes(x = 'age'))
)

print(fig)

# line plot


x = np.random.uniform(size=(10))
y = x ** 2 + 0.2 * x

data = {'x': x, 'y': y}
df = pd.DataFrame(data)
print(df.head(2))
fig = (ggplot(df, aes(x=x,y=y)) + geom_line())
print(fig)

#SCATTER PLOTS AND SMOOTHING CURVES
#%%
custdata2 = custdata[(custdata.age > 0) & (custdata.age < 100) & (custdata.income > 0)]
print(custdata2.head(10))
print(custdata2.income)
print(custdata2.age)

# corelation between age and income in pandas
corr = custdata2.age.corr(custdata2.income, method="pearson")
print(corr)

fig = (ggplot(custdata2, aes(x= 'age', y= 'income')) + geom_point() +
       ylim(0,200000))

print(fig)

## SCATTER

fig = (ggplot(custdata2, aes(x= 'age', y= 'income')) + geom_point() +
       stat_smooth(method="lm") + ylim(0,200000))

print(fig)
