import pandas as pd
import numpy as np
import pandas_profiling
import matplotlib.pyplot as plt
import seaborn as sns

# Reading data from a tab separated file using pandas library creates a Data Frame
custdata = pd.read_csv('/home/hariactyv/Machine_Learning/PDS_master/data/custdata.tsv',
                       delimiter='\t')

# names of columns of Data Frame
print(custdata.columns)

# Statistical summary of each column of Data Frame
print(custdata.describe(include='all'))

# Basic data type, counts and missing values summary of columns of Data Frame
print(custdata.info(null_counts=True, verbose=True))

# Number of NaNs in columns 'housing.type', 'recent.move',
print(custdata['housing.type'].isna().sum(),custdata['recent.move'].isna().sum(),
      custdata['num.vehicles'].isna().sum())

# mapping is.employed column to logical meaning
custdata['is.employed.fix'] = custdata['is.employed'].copy()

# Cannot map NaN or 'NaN' so changing NaN to missing
custdata.loc[custdata['is.employed.fix'].isnull(), 'is.employed'] = 'missing'
fix = { 'missing' : 'missing',
       True  : "employed",
       False : "not employed"}
print(custdata['is.employed'].value_counts().index)
print(custdata['is.employed'].value_counts().values)
# print(custdata['is.employed'])
custdata['is.employed.fix'] = custdata['is.employed'].map(fix)
print(custdata['is.employed.fix'])



