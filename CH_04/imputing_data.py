import pandas as pd
import pandas_profiling

# Read data


df = pd.read_csv('train.csv')

# profile = pandas_profiling.ProfileReport(df)
# profile.to_file(output_file='./report.html')

# summary and infomation of data
print(df.head())
print(df.describe(include='all'))
print(df.info())
print(df.columns)
print(df.isnull().sum())
print('-x-' * 30)
print()
# 1. Create subset of the data to work with
#
#     LotFrontage: Linear feet of street connected to property
#     FireplaceQu: Fireplace quality
#     GarageYrBlt: Year garage was built
#     BsmtCond: General condition of the basement


housing = df[['LotFrontage','FireplaceQu','GarageYrBlt','BsmtCond']].copy()


print(housing.head())
print(housing.describe(include='all'))
print(housing.info())
print('-x-' * 30)
print()

##2 Examine missing data

print(housing.isnull().sum())
print('-x-' * 30)
print()

##3. percentage of missing data in each column

print((housing.isnull().sum()/len(housing)) * 100)
print('-x-' * 30)
print()


## 4 Drop columns with more than 25% of missing data

housing.drop('FireplaceQu',inplace=True,axis=1)
print(housing.head(2))
print('-x-' * 30)
print()

## Impute Substitute Values
# Strategy 1: Impute Mean


garage_yr_mean = housing['GarageYrBlt'].mean()

print(garage_yr_mean)
print('-x-' * 30)
print()

housing['GarageYrBlt'].fillna(garage_yr_mean,inplace=True)

## Strategy 2: Impute Median

frontage_median = housing['LotFrontage'].median()

print(frontage_median)
print('-x-' * 30)
print()

housing['LotFrontage'].fillna(frontage_median,inplace=True)

## Strategy 3: Impute Mode

print(housing['BsmtCond'].value_counts())
print('-x-' * 30)
print()
bsmt_cond_mode = housing['BsmtCond'].value_counts().index[0]

print(bsmt_cond_mode)
print('-x-' * 30)
print()
housing['BsmtCond'].fillna(bsmt_cond_mode,inplace=True)

## Check for missing data

print(housing.isnull().sum())
print('-x-' * 30)
print()

## pandas ffill and bfill
##  Use ffill() function to fill the missing values along the row axis
## When ffill() is applied across the index then any missing value is filled based
# on the corresponding value in the previous row.
print(df['BsmtExposure'].isnull().sum())
print(df['BsmtExposure'].value_counts())
df['BsmtExposure'].fillna(method='ffill',inplace=True)
# or
# df.loc[:,'BsmtExposure'] = df.loc[:,'BsmtExposure'].ffill()

print(df['BsmtExposure'].isnull().sum())
print('-x-' * 30)
print()

## Use bfill() function to populate missing values na values in the dataframe across rows
## When axis='rows', then value in current na cells are filled from the corresponding
## value in the next row. If the next row is also na value then it won’t be populated.
print(df['BsmtFinType1'].isnull().sum())
print(df['BsmtFinType1'].value_counts())
df.loc[:,'BsmtFinType1'] = df.loc[:,'BsmtFinType1'].bfill()
print(df['BsmtFinType1'].isnull().sum())
print('-x-' * 30)
print()






