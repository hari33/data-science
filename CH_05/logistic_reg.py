import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.metrics import confusion_matrix
import shap

shap.initjs()
## 1. reading data from a tab separated file
spam = pd.read_csv('/home/hariactyv/Test_R/PDSwR2-master/Spambase/spamD.tsv', delimiter='\t')


print(spam.head(10))
print(spam.dtypes)
print(spam.columns)
# spliting of data into train and test
spamTrain = spam.loc[spam.rgroup >= 10,]
spamTest = spam.loc[spam.rgroup < 10,]

# creation and encoding of train and test labels
lb = LabelEncoder()
TrainLabel = lb.fit(spamTrain['spam']).transform(spamTrain['spam'])
TestLabel = lb.transform(spamTest['spam'])


# dropping label column from train and test datasets
spamTrain = spamTrain.drop(['spam'], axis = 1)
spamTest = spamTest.drop(['spam'], axis = 1)
features = spamTrain.columns

# Scaling all columns to same scale using normalizing technique
Sc = StandardScaler()
spamTrain = Sc.fit(spamTrain).transform(spamTrain)
spamTest = Sc.transform(spamTest)

print('Train Data shape {} '.format(spamTrain.shape))
print('Train Label shape {} '.format(TrainLabel.shape))

print('Test Data shape {} '.format(spamTest.shape))
print('Test Label shape {} '.format(TestLabel.shape))


logreg = LogisticRegression(C=1e9)
result =logreg.fit(spamTrain, TrainLabel)

TestPred = logreg.predict(spamTest)

## accuracy
print('Accuracy of logistic regression classifier on test set: {:.2f}'
      .format(logreg.score(spamTest, TestLabel)))

# Confusion Matrix
cm = confusion_matrix(TestLabel, TestPred)
print("confusion matrix of logistic regression")
print(cm)


from sklearn.metrics import classification_report
print(classification_report(TestLabel, TestPred))



# preccision, recall and f1-score
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
logit_roc_auc = roc_auc_score(TestLabel, TestPred)
fpr, tpr, thresholds = roc_curve(TestLabel, logreg.predict_proba(spamTest)[:,1])
plt.figure()
plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
#plt.savefig('Log_ROC')
plt.show()

print(result.coef_)
print(result.intercept_)

## to get statistical result like R

import statsmodels.api as sm
logit_model=sm.Logit(TrainLabel,spamTrain )
result=logit_model.fit()
print(result.summary())
print(result.summary2())
#spamTrain = pd.DataFrame(spamTrain)
#spamTest = pd.DataFrame(spamTest)


# use Kernel SHAP to explain test set predictions
explainer = shap.LinearExplainer(logreg, spamTrain, feature_dependence="independent")
shap_values = explainer.shap_values(spamTest)
#spamTest_array = spamTest.toarray() # we need to pass a dense version for the plotting functions
shap.summary_plot(shap_values, spamTest, feature_names=features)

shap.initjs()
explainer = shap.KernelExplainer(logreg.predict_proba, spamTrain)#, feature_dependence="independent")
shap_values = explainer.shap_values(spamTest[0,:])
#spamTest_array = spamTest.toarray() # we need to pass a dense version for the plotting functions
shap.force_plot(explainer.expected_value[0], shap_values[0], spamTest[0,:],matplotlib=True)
