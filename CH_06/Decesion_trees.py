from sklearn.datasets import load_iris
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.tree import export_graphviz
#from sklearn.externals.six import StringIO
from IPython.display import Image
from pydot import graph_from_dot_data
import pandas as pd
from io import StringIO
import numpy as np

iris = load_iris()
X = pd.DataFrame(iris.data, columns=iris.feature_names)
y = pd.Categorical.from_codes(iris.target, iris.target_names)

##  one hot encoding
y = pd.get_dummies(y)
print(y)

## train and test split
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)

## training and predicting
dt = DecisionTreeClassifier()
dt.fit(X_train, y_train)
y_pred = dt.predict(X_test)

## viewing the actual tree

dot_data = StringIO()
export_graphviz(dt, out_file=dot_data, feature_names=iris.feature_names)
(graph,) = graph_from_dot_data(dot_data.getvalue())
#Image(graph.create_png())
graph.write_pdf("iris.pdf")

## confusion matrix
species = np.array(y_test).argmax(axis=1)
predictions = np.array(y_pred).argmax(axis=1)
cm = confusion_matrix(species, predictions)
print('Accuracy of Decesion Tree is ', np.round(dt.score(X_test,y_test) * 100,2), ' %')
print(cm)
print(classification_report(y_test,y_pred))