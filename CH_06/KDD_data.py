import gzip
import pandas as pd
import numpy as np
import gc
import random

# reading data and creating new data frame columns from another data frame
with gzip.open('/home/hariactyv/Test_R/PDSwR2-master/KDD2009/orange_small_train.data.gz') as f:
    data = pd.read_csv(f, delimiter='\t')
print(data.head(10))
print(data.shape)

churn = pd.read_csv('/home/hariactyv/Test_R/PDSwR2-master/KDD2009/orange_small_train_churn.labels.txt',
                    delimiter='\t', header=None)
data['churn'] = churn

appetency = pd.read_csv('/home/hariactyv/Test_R/PDSwR2-master/KDD2009/orange_small_train_appetency.labels.txt',
                        delimiter='\t', header=None)
data['appetency'] = appetency

upselling = pd.read_csv('/home/hariactyv/Test_R/PDSwR2-master/KDD2009/orange_small_train_upselling.labels.txt',
                        delimiter='\t', header=None)
data['upselling'] = upselling

print(data.head(10))
print(data.shape)

# Spliting   data into train and test using  threshold
random.seed(42)
data['rgroup'] = np.random.uniform(0,1, data.shape[0])
print(data.rgroup.size)
print(data.rgroup)

dataTrainAll = data.loc[data['rgroup'] <= 0.9,]
dataTest = data.loc[data['rgroup'] > 0.9,]

print(data.dtypes)

# determining catogarical and numeric variables in the data frame excluding the modeling columns
outcomes =['churn','appetency','upselling','rgroup']
vars = set(data.columns).difference(set(outcomes))
print(len(vars))
catvars  =  data[vars].select_dtypes(include=['category','object']).columns.tolist()
numvars = data[vars].select_dtypes(include=['number']).columns.tolist()
print(data.Var200.describe())
print(len(catvars))
print(len(numvars))
del [[data, churn, appetency, upselling]]
gc.collect()
outcome = 'churn'
pos = 1

# spliting train data into cal and train using thresholding
useForcal = np.random.binomial(n=dataTrainAll.shape[0], p=0.1, size=1) > 0
print(len(useForcal))
useForcal = int(useForcal == 'True')
print(useForcal)

datacal = dataTrainAll.iloc[:useForcal]
dataTrain = dataTrainAll.iloc[useForcal:,]
print(datacal.shape)
print(dataTrain.shape)

# Plotting churn grouped by variable 218 levels
values = {'Var218': '<NA>'}
dataTrain = dataTrain.fillna(values)
print(dataTrain.pivot_table(index='Var218', columns=outcome,aggfunc={'Var218':len}, dropna=False))

# Churn rates grouped by variable 218 codes
table =pd.crosstab(dataTrain.Var218,dataTrain.churn, dropna=False)
print(type(table))
print(table.shape)
results = table[1]/(table[-1] + table[1])
print(results)

# single variable memorization
def mkPredC(df,outCol,varCol,appCol):

    pPos = sum((df[outCol]==pos).values)/ len(df[outCol])

    df[varCol] = df[varCol].fillna('<NA>')
    # < -- not required -->
    naTab = pd.crosstab(df[varCol].loc[df[varCol]=='<NA>'], df[outCol], dropna=False)
    if pos in naTab.columns:
        pPosWna = naTab[pos]/naTab.values.sum()
    else:
        pPosWna = 0
    df[varCol] = df[varCol].replace('<NA>', np.nan)
    # < -- till here --
    vTab = pd.crosstab(df[varCol], df[outCol])
    if pos in vTab.columns:
        pPosWv = (vTab[pos]+1.0e-3*pPos)/(vTab.values.sum()+1.0e-3)
    else:
        pPosWv = 0

    pred = pPosWv[df[appCol].fillna('')]
    #pred[pd.isna(df[appCol])] = pPosWna
    pred = pred.reset_index(drop=True)
    pred[pd.isna(pred)] = pPos
    return  pred
    #print(pred)

    #naTab[pos]/(naTab.values.sum())
    #pPosWna = naTab[:,pos]/

    #naTab <- table(as.factor(outCol[is.na(varCol)])
    #naTab = outCol.iloc[varCol.isna()==True]
    #values = { 'NAN' : '<NA>'}
    #varCol = varCol.fillna('<NA>')
    #naTab = pd.crosstab(varCol, outCol, dropna=False)
    # print(type(table))
    # print(table.shape)
    # results = table[1]/(table[-1] + table[1])
    #naTab = outCol[varCol==pd.isna(varCol)]
   # print(naTab)
   #  pPosWna = (naTab/sum(naTab))[pos]
   #
   #  vTab = pd.crosstab(outCol,varCol)
   #  pPosWv = (vTab[pos,]+1.0e-3*pPos)/(vTab.sum()+1.0e-3)
   #  pred = pPosWv[appCol]
   #  pred[pd.isna(appCol)] = pPosWna
   #  pred[pd.isna(pred)] = pPos


for v in catvars:
    print(type(v))
    pi = 'pred_{}'.format(v)
    print(pi)
    dataTrain[pi] = mkPredC(dataTrain, outcome, v, v)
    # datacal[pi] = mkPredC(datacal, outcome, v, v)
    dataTest[pi] = mkPredC(dataTest, outcome, v, v)

    #dataTrain[:,pi] = mkPredC(dataTrain.loc[:,[outcome]],dataTrain.loc[:,[v]],dataTrain.loc[:,[v]])
# dCal[,pi] <- mkPredC(dTrain[outcome],dTrain[v],dCal[v])
# dTest[,pi] <- mkPredC(dTrain[,outcome],dTrain[,v],dTest[,v])