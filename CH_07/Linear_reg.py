from plotnine import *
import pandas as pd
import pandas_profiling
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import numpy as np

'''
# code used to read RDS file and save it as csv
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri
pandas2ri.activate()
readRDS = robjects.r['readRDS']
df = readRDS('/home/hari/TEST_R/PDSwR2-master/PUMS/psub.RDS')
#df = pandas2ri.rpy2py_dataframe(df)
df.to_csv('psub1.csv',index=False)
print(type(df))
'''



df = pd.read_csv('../data/psub.csv',delimiter=',', low_memory=False, skipinitialspace=True)
#profile = pandas_profiling.ProfileReport(df)
#profile.to_file(output_file='./data_summary.html')
#print(df['pincp'],df['sex'],df['cow'],df['agep'],df['schl'])

# creating a subset data  from the the original data
# 1. assignning the independent column names required

names = ['sex','cow','agep','schl']
data = df[names]
print('Dependent data details')
print(data.head(10))
print(data.shape)
print(data.describe())
print('-x-' * 50)
print()

# visualizing  data
plt.hist(data['cow'],)
plt.show()


# 3. assignning the dependent variable required by tranforming the data to log base 10
target = np.log10(df['pincp'])
print('Target data details')
print(target.head(10))
print(target.shape)
print(target.describe())
print(target.isna().sum())
print('-x-' * 50)
print()

# visualizing  distribution of target data
import seaborn as sns
plt.figure(figsize=(15,10))
plt.tight_layout()
sns.distplot(target)
plt.show()

# 4. creating test and train datasets
from sklearn.model_selection import train_test_split
data_train, data_test, target_train, target_test = train_test_split(data, target,
                                                                    test_size=0.33,
                                                                    random_state=42)
print('training data shape : ',data_train.shape)
print('training target shape : ',target_train.shape)
print('test data shape : ',data_test.shape)
print('test data shape : ',target_test.shape)
print('-x-' * 50)
print()

# 5. rescaling the data to the same scale using standard scaling
## scaled feature = (feature - mean of feature)/standard deviation of feature
## feature gets rescaled between  0  and 1.
from sklearn.preprocessing import  StandardScaler
Sc = StandardScaler()
print(type(data_train))
data_train_center = Sc.fit_transform(data_train)
data_test_center = Sc.transform(data_test)

print(type(target_train))
## have to convert to data frame because Standard scaling
## cannot be done to A pandas sieries object
target_train_center = Sc.fit_transform(pd.DataFrame(target_train))
target_test_center = Sc.transform(pd.DataFrame(target_test))

##6. implementing linear regression
linreg = LinearRegression()
linreg.fit(data_train_center,target_train_center)

##7. predicting the target for input of data_test_center
target_pred = linreg.predict(data_test_center)
print(type(target_test_center))
print(type(target_pred))

## compare  predicted and actual targets and visualize
df = pd.DataFrame({'Actual': target_test_center.flatten(),
                        'Predicted': target_pred.flatten()})
print(df)

df1 = df.head(25)

df1.plot(kind='bar',figsize=(10,8))
plt.grid(which='major', linestyle='-', linewidth='0.5', color='green')
plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
plt.show()

##7. viewing the intercept and coefficients
print('intercept : ',linreg.intercept_)
coeff_df = pd.DataFrame(linreg.coef_.reshape(-1,1), data.columns, columns=['Coefficient'])
print(coeff_df)
print('-x-' * 50)

#8. viewing the  the different measures of error
from sklearn import metrics
print('Mean Absolute Error:', metrics.mean_absolute_error(target_test_center, target_pred))
print('Mean Squared Error:', metrics.mean_squared_error(target_test_center, target_pred))
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(target_test_center, target_pred)))


# 9. Explained variance score: 1 is perfect prediction
from sklearn.metrics import r2_score
print('Variance score: %.2f' % r2_score(target_test_center, target_pred))

## extras
from plotnine import *
fig = (ggplot(df, aes(x= 'Predicted',y= 'Actual')) +
       geom_point(alpha=0.2,color="black") +
        geom_smooth(method='lm') +
        xlab('Actual') + ylab('Predicted'))

print(fig)

## to get statistical result like R

import statsmodels.api as sm
model = sm.OLS(target_train_center, data_train_center).fit()
print(model.summary())
print(model.summary2())


