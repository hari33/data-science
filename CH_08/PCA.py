import  pandas as pd


## example 1
url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"

# load dataset into Pandas DataFrame Using url
df = pd.read_csv(url, names=['sepal length','sepal width','petal length','petal width',
                             'target'])

print(df.head(5))
print(df.shape)

from sklearn.preprocessing import StandardScaler

features = ['sepal length', 'sepal width', 'petal length', 'petal width']

# Separating out the features
x = df.loc[:, features].values

# Separating out the target
y = df.loc[:,['target']].values

# Standardizing the features
x = StandardScaler().fit_transform(x)

# original data has 4 features
# reducing the features to 2 principal components
from sklearn.decomposition import PCA

pca = PCA(n_components=2)
principalComponents = pca.fit_transform(x)

## printing the variance explained by the two principal components
print(pca.explained_variance_ratio_)
principalDf = pd.DataFrame(data = principalComponents,
                           columns = ['principal component 1', 'principal component 2'])


print(principalDf)

finalDf = pd.concat([principalDf, df[['target']]], axis = 1)

import matplotlib.pyplot as plt

##  2-D Visualization of two princpal compomonents

fig = plt.figure(figsize = (8,8))
ax = fig.add_subplot(1,1,1)
ax.set_xlabel('Principal Component 1', fontsize = 15)
ax.set_ylabel('Principal Component 2', fontsize = 15)
ax.set_title('2 component PCA', fontsize = 20)
targets = ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']
colors = ['r', 'g', 'b']
for target, color in zip(targets,colors):
    indicesToKeep = finalDf['target'] == target
    ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
               , finalDf.loc[indicesToKeep, 'principal component 2']
               , c = color
               , s = 50)
ax.legend(targets)
ax.grid()
plt.show()

## exmaple 2

## reading mnist train and test csv files
## extracting the features and target from the data frame using python slicing technique

# reading training data
train = pd.read_csv('mnist_train.csv', delimiter=',', )
print(train.head(5))

## extracting features  and target using python slicing technique
train_img = train.iloc[:,1:]
train_lbl = train.iloc[:,0]
print(train_img.shape)
print(train_lbl.shape)

# reading training data
test = pd.read_csv('mnist_test.csv', delimiter=',', )
print(train.head(5))

## extracting features  and target using python slicing technique
test_img = test.iloc[:,1:]
test_lbl = test.iloc[:,0]
print(test_img.shape)
print(test_lbl.shape)

##
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()# Fit on training set only.
scaler.fit(train_img)# Apply transform to both the training set and the test set.
train_img = scaler.transform(train_img)
test_img = scaler.transform(test_img)

## Notice the code below has .95 for the number of components parameter.
# It means that scikit-learn choose the minimum number of principal components
# such that 95% of the variance is retained.

from  sklearn.decomposition import PCA

## Make an instance of the Model
pca = PCA(.95)

##  Note: you are fitting PCA on the training set only.
pca.fit(train_img)


## Apply the mapping (transform) to both the training set and the test set.
train_img = pca.transform(train_img)
test_img = pca.transform(test_img)

## applying logistic regression to tranformed data

##Step 1: Import the model you want to use

from sklearn.linear_model import LogisticRegression

## Step 2: Make an instance of the Model.

# all parameters not specified are set to their defaults
# default solver is incredibly slow which is why it was changed to 'lbfgs'

logisticRegr = LogisticRegression(solver = 'lbfgs')

## Step 3: Training the model on the data, storing the information learned from the data

## Model is learning the relationship between digits and labels

logisticRegr.fit(train_img, train_lbl)

## Step 4: Predict the labels of new data (new images)

# Predict for One Observation (image)

print(logisticRegr.predict(test_img[0].reshape(1,-1)))

##The code below predicts for multiple observations at once


print(logisticRegr.predict(test_img[0:10]))

## Measuring Model Performance


print("accuracy : ", logisticRegr.score(test_img, test_lbl))


