import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans

# generate some random cluster data
X, y = make_blobs(random_state=170, n_samples=600, centers = 5)
rng = np.random.RandomState(74)

# transform the data to be stretched
transformation = rng.normal(size=(2, 2))
X = np.dot(X, transformation)# plot
plt.scatter(X[:, 0], X[:, 1])
plt.xlabel("Feature 0")
plt.ylabel("Feature 1")
plt.show()


# cluster the data into five clusters
## elbow plot used to decide the number of clusters in kmeans clusterring

distortions = []
for i in range(1, 11):
    km = KMeans(n_clusters=i,
                init='k-means++',
                n_init=10,
                max_iter=300,random_state=0)
    km.fit(X)
    distortions.append(km.inertia_)
plt.plot(range(1,11), distortions, marker='o')
plt.xlabel('Number of clusters')
plt.ylabel('Distortion')
plt.show()

kmeans = KMeans(n_clusters=5, init='k-means++')
kmeans.fit(X)

y_pred = kmeans.predict(X)

# plot the cluster assignments and cluster centers
plt.scatter(X[:, 0], X[:, 1], c=y_pred, cmap="plasma")
plt.scatter(kmeans.cluster_centers_[:, 0],
            kmeans.cluster_centers_[:, 1],
            marker='^',
            c=[0, 1, 2, 3, 4],
            s=100,
            linewidth=2,
            cmap="plasma")

plt.xlabel("Feature 0")
plt.ylabel("Feature 1")
plt.show()

## DBSCAN 'density based spatial clustering of applications with noise'
## does not require the user to set the number of clusters a priori
## can capture clusters of complex shapes
## can identify points that are not part of any cluster (very useful as outliers detector)
## is somewhat slower than agglomerative clustering and k-means, but still scales to relatively large datasets.
## works by identifying points that are in crowded regions of the feature space, where many data points are close together (dense regions in feature space)
## Points that are within a dense region are called core samples (or core points)
## There are two parameters in DBSCAN: min_samples and eps
## If there are at least min_samples many data points within a distance of eps to a given data point, that data point is classified as a core sample
## core samples that are closer to each other than the distance eps are put into the same cluster by DBSCAN.



from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_scaled = scaler.fit_transform(X)

# cluster the data into five clusters
dbscan = DBSCAN(eps=0.123, min_samples = 2)
clusters = dbscan.fit_predict(X_scaled)
# plot the cluster assignments
plt.scatter(X[:, 0], X[:, 1], c=clusters, cmap="plasma")
plt.xlabel("Feature 0")
plt.ylabel("Feature 1")
plt.show()