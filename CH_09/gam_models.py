'''
Implementation of generalized additive models in python using pygam LinearGAM comparing
with statsmodels api ols ie.ordinary least squares linear regression
for more on pygam read https://buildmedia.readthedocs.org/media/pdf/pygam/latest/pygam.pdf
'''
import statsmodels.api as sm
from plotnine import *
import pandas as pd
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

import pyreadr
from pygam import LinearGAM, LogisticGAM
# rdata file in python
rdata = pyreadr.read_r('/home/hariactyv/Test_R/PDSwR2-master/CDC/NatalBirthData.rData')
print(rdata.keys())
data = rdata['sdata']
print(type(data))
print(data.head(10))

# defining input variables, target variable and
# creating input and target data for defined variables
intput_features = ['PWGT', 'WTGAIN', 'MAGER', 'UPREVIS']
target_feature = ['DBWT']

inputdata = data[intput_features]
print(inputdata.head(10))
print(inputdata.shape)
print(inputdata.describe())

targetdata = data[target_feature]
print(targetdata.head(10))
print(targetdata.shape)
print(targetdata.describe())

# splitting the data into train
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(inputdata, targetdata, test_size=0.33, random_state=42)

X = inputdata
y = targetdata

X = sm.add_constant(X) # adding a constant

model = sm.OLS(y, X).fit()
predictions = model.predict(X)

print_model = model.summary()
print(print_model)
print('-X-' * 30)

gam = LinearGAM(n_splines=10).fit(inputdata, targetdata)
print(gam.summary())


for i, term in enumerate(gam.terms):
    if term.isintercept:
        continue

    XX = gam.generate_X_grid(term=i)
    pdep, confi = gam.partial_dependence(term=i, X=XX, width=0.95)
    plt.figure()
    plt.plot(XX[:, term.feature], pdep)
    plt.plot(XX[:, term.feature], confi, c='r', ls='--')
    plt.title(repr(term))
    plt.show()