# Environment

1. Anaconda Python 3.6  environment. https://docs.anaconda.com/anaconda/install/  
2. Create conda python3 virtual env named pytorch.  
3. Install pytorch in env. https://pytorch.org/get-started/locally/


## Tools
1. Pycharm : https://www.jetbrains.com/pycharm/  
  
## **Reference**   
1. Practical Data Science with R by  Nina Zumel & John Mount