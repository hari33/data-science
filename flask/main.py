import os
import sys
import re
from PIL import Image
from app import app
from flask import Flask, flash, request, redirect, render_template
from werkzeug.utils import secure_filename
import numpy as np
sys.path.append("/home/hari/CarLoan/model/")
import tensorflow as tf
from load import *

#global vars for easy reusability
global model, graph
#initialize these variables
model, graph = init()

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def upload_form():
    return render_template('upload.html')

@app.route('/', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No file selected for uploading')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            read_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            #read the image into memory
            x = Image.open(read_path).convert('L')
            #make it the right size
            x_new = x.resize((500,500))
            print("debug")
            #convert to a 4D tensor to feed into our model
            x_new = np.array(x_new)
            #x_new = np.expand_dims(x_new,2)
            #x_new = np.expand_dims(x_new,0)
            x_new = x_new.reshape([1,500,500,1])
            #x_new =  tf.convert_to_tensor(x_new)
            print("debug1")
            #in our computation graph
            with graph.as_default():
                #perform the prediction
                y_pred = model.predict(x_new)
                print(y_pred)
                print(np.argmax(y_pred,axis=1))
                print ("debug3")
                #convert the response to a string
                response  = np.array_str(np.argmax(y_pred,axis=1))
                #response = np.argmax(y_pred,axis=1).tolist()
                print(type(response))
                response = str(response).strip('[]')
                print(type(response))
                if response == '0':
                    response = 'invalid'
                else:
                    response = 'valid'
            #flash('File successfully uploaded')
            os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            #flash(response)
            #return redirect('/')
            return response
        else:
            flash('Allowed file types are txt, pdf, png, jpg, jpeg, gif')
            return redirect(request.url)


if __name__ == "__main__":
    app.run()