import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from plotnine import *

def logistic(data_train_center, data_test_center, target_train_center, target_test_center, target, data):
    ## visualizing  distribution of target data
    import seaborn as sns
    sns.distplot(target, rug=True, rug_kws={"color": "g"},
                 kde_kws={"color": "r", "lw": 3, "label": "KDE"},
                 hist_kws={"histtype": "step", "linewidth": 3,
                           "alpha": 1, "color": "g"})

    plt.title("Distribition of Target")
    plt.show()


    ## feature selection
    ## Notice the code below has .95 for the number of components parameter.
    ## It means that scikit-learn choose the minimum number of principal components
    ## such that 95% of the variance is retained.

    from  sklearn.decomposition import PCA
    pca = PCA(0.95)

    ##  Note: you are fitting PCA on the training set only.
    model = pca.fit(data_train_center)

    ## Apply the mapping (transform) to both the training set and the test set.
    data_train_center= model.transform(data_train_center)
    data_test_center = model.transform(data_test_center)
    print('pca explained_variance_ratio_')
    print(model.explained_variance_ratio_)

    # number of components
    n_pcs= model.components_.shape[0]

    # get the index of the most important feature on EACH component i.e. largest absolute value
    # using LIST COMPREHENSION HERE
    most_important = [np.abs(model.components_[i]).argmax() for i in range(n_pcs)]

    initial_feature_names = data.columns

    # get the names
    most_important_names = [initial_feature_names[most_important[i]] for i in range(n_pcs)]

    # using LIST COMPREHENSION HERE AGAIN
    dic = {'PC{}'.format(i+1): most_important_names[i] for i in range(n_pcs)}

    # build the dataframe
    df = pd.DataFrame((dic.items()),model.explained_variance_ratio_,columns=['Principal_Componenet', 'Feature'])
    print(df)

    ## implementing linear regression
    from sklearn.linear_model import LogisticRegression
    logreg = LogisticRegression(C=1e9)
    logreg.fit(data_train_center,target_train_center)


    ## predicting the target for input of data_test_center
    target_pred = logreg.predict(data_test_center)
    print(type(target_test_center))
    print(type(target_pred))

    # ## compare  predicted and actual targets and visualize
    # AP = pd.DataFrame({'Actual': target_test_center,
    #                    'Predicted': target_pred.flatten()})
    # print(AP)
    #
    # df1 = AP.head(25)
    #
    # df1.plot(kind='bar',figsize=(10,8))
    # plt.grid(which='major', linestyle='-', linewidth='0.5', color='green')
    # plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
    # plt.show()

    ## viewing the intercept and coefficients
    print('intercept : ',logreg.intercept_)
    data = pd.DataFrame(data_train_center)
    coeff_df = pd.DataFrame(logreg.coef_.reshape(-1,1), most_important_names, columns=['Coefficient'])
    print(coeff_df)
    print('-x-' * 50)

    ## accuracy
    print('Accuracy of logistic regression classifier on test set: {:.2f}'
          .format(logreg.score(data_test_center, target_test_center)))

    from mlxtend.evaluate import confusion_matrix
    from mlxtend.plotting import plot_confusion_matrix
    cm = confusion_matrix(y_target=target_test_center,y_predicted=target_pred,binary=False)
    fig, ax = plot_confusion_matrix(cm)
    plt.show()




    # Confusion Matrix
    from sklearn.metrics import confusion_matrix
    cm = confusion_matrix(target_test_center, target_pred)
    print("confusion matrix of logistic regression")
    print(cm)
    # class_names=logreg.classes_
    #
    # np.set_printoptions(precision=2)
    #
    # # Plot non-normalized confusion matrix
    # plot_confusion_matrix(target_test_center,target_pred, classes=class_names, normalize=True,
    #                   title='Confusion matrix, without normalization')
    # plt.show()

    from sklearn.metrics import classification_report
    print(classification_report(target_test_center, target_pred))

    # ROC
    from sklearn.metrics import roc_auc_score
    from sklearn.metrics import roc_curve
    logit_roc_auc = roc_auc_score(target_test_center, target_pred)
    fpr, tpr, thresholds = roc_curve(target_test_center, logreg.predict_proba(data_test_center)[:,1])
    plt.figure()
    plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    #plt.savefig('Log_ROC')
    plt.show()


    ## to get statistical result like R
    import statsmodels.api as sm
    logit_model=sm.Logit(target_train_center,data_train_center )
    result=logit_model.fit()
    print(result.summary())
    print(result.summary2())

    # use Kernel SHAP to explain test set predictions
    import shap
    explainer = shap.LinearExplainer(logreg, data_train_center, feature_dependence="independent")
    shap_values = explainer.shap_values(data_test_center)
    #spamTest_array = spamTest.toarray() # we need to pass a dense version for the plotting functions
    shap.summary_plot(shap_values, data_test_center, feature_names=initial_feature_names)

    shap.initjs()
    explainer = shap.KernelExplainer(logreg.predict_proba, data_train_center)#, feature_dependence="independent")
    shap_values = explainer.shap_values(data_test_center[0,:])
    #spamTest_array = spamTest.toarray() # we need to pass a dense version for the plotting functions
    shap.force_plot(explainer.expected_value[0], shap_values[0], data_test_center[0,:],matplotlib=True)

'''
def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    from sklearn.metrics import confusion_matrix
    from sklearn.utils.multiclass import unique_labels
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax




Plot normalized confusion matrix
plot_confusion_matrix(y_test, y_pred, classes=class_names, normalize=True,
                      title='Normalized confusion matrix')
'''
