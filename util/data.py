import pandas as pd
import numpy as np


## function for imputing datetime columns
## imputing methods allowed are ['missing', 'mode', 'ffill','bfill']
def data_preprocess_datetime(df, imp_method=None):
    if imp_method  in ['drop','linear','ffill','bfill']:
        pass
    else: raise  NotImplementedError("Method Not Allowed")
    for col in df.columns:
        if df[col].dtype == 'datetime64[ns]':
            if (df[col].isnull()).any():
                if df[col].isnull().sum()/len(df[col]) * 100 > 25:
                    df.drop([col], axis=1, inplace=True)
                elif imp_method == 'drop':
                    df.dropna(subset=[col],inplace=True)
                elif imp_method == 'linear':
                    df[col].interpolate(method='linear', inplace=True)
                elif imp_method == 'ffill':
                    df[col].fillna(method='ffill', inplace=True)
                elif imp_method == 'bfill':
                    df[col].fillna(method='bfill', inplace=True)
    for col in df.columns:
        if df[col].dtype == 'datetime64[ns]':
            df[col] = df[col].astype(np.int64) // 10**9


    print(df.shape)
    return df




## function for imputing object columns
## imputing methods allowed are ['drop', 'missing', 'ffill','bfill']
def data_preprocess_string(df, imp_method=None):
    if imp_method  in ['missing','ffill','bfill']:
        pass
    else: raise  NotImplementedError("Method Not Allowed")
    for col in df.columns:
        if df[col].dtype == 'object':
            if (df[col].isnull()).any():
                if df[col].isnull().sum()/len(df[col]) * 100 > 25:
                    df.drop([col], axis=1, inplace=True)
                elif imp_method == 'missing':
                    df[col].fillna('missing', inplace=True)
                elif imp_method == 'ffill':
                    df[col].fillna(method='ffill', inplace=True)
                elif imp_method == 'bfill':
                    df[col].fillna(method='bfill', inplace=True)

    ## Label Encoding
    from sklearn.preprocessing import LabelEncoder
    le = LabelEncoder()
    # Categorical boolean mask
    categorical_feature_mask = df.dtypes== 'object'
    # filter categorical columns using mask and turn it into a list
    categorical_cols = df.columns[categorical_feature_mask].tolist()
    print(categorical_cols)
    # apply le on categorical feature columns
    df[categorical_cols] = df[categorical_cols].apply(lambda col: le.fit_transform(col))
    print(df.shape)
    return df

## function for imputing numeric columns
## imputing methods allowed are ['mean', ,median', 'mode', 'max', 'min', '1_quantile',
## , '3_quqntile', 'ffill','bfill']

def data_preprocess_numeric(df, imp_method=None):
    if imp_method  in ['mean', 'median', 'mode', 'max', 'min', '1_quantile', '3_quqntile' 'ffill','bfill']:
        pass
    else: raise  NotImplementedError("Method Not Allowed")

    for col in df.columns:
        if df[col].dtype == 'int64' or df[col].dtype == 'float64':
            if (df[col].isnull()).any():
                if df[col].isnull().sum()/len(df[col]) * 100 > 25:
                    df.drop([col], axis=1, inplace=True)
                elif imp_method == 'mean':
                    value = df[col].mean()
                    df[col].fillna(value, inplace=True)
                elif imp_method == 'median':
                    value = df[col].median()
                    df[col].fillna(value, inplace=True)
                elif imp_method == 'mode':
                    value = df[col].mode()
                    df[col].fillna(value, inplace=True)
                elif imp_method == 'max':
                    value = df[col].max()
                    df[col].fillna(value, inplace=True)
                elif imp_method == 'min':
                    value = df[col].min()
                    df[col].fillna(value, inplace=True)
                elif imp_method == '1_quantile':
                    value = df[col].quantile(0.25)
                    df[col].fillna(value, inplace=True)
                elif imp_method == '3_quantile':
                    value = df[col].quantile(0.75)
                    df[col].fillna(value, inplace=True)
                elif imp_method == 'ffill':
                    df[col].fillna(method='ffill', inplace=True)
                elif imp_method == 'bfill':
                    df[col].fillna(method='bfill', inplace=True)

    for col in df.columns:
        if df[col].dtype == 'int64' or df[col].dtype == 'float64':
            if df[col].std()==0:
                df.drop([col], axis=1, inplace=True)
    print(df.shape)
    return df




def data_split_regression(df, target_column):#,transform=None):
    target = df[target_column]
    data = df.drop([target_column], axis=1)

    # creating test and train datasets
    from sklearn.model_selection import train_test_split
    data_train, data_test, target_train, target_test = train_test_split(data, target,
                                                                    test_size=0.33,
                                                                    random_state=42)
    print('training data shape : ',data_train.shape)
    print('training target shape : ',target_train.shape)
    print('test data shape : ',data_test.shape)
    print('test data shape : ',target_test.shape)
    print('-x-' * 50)
    print()

    # 5. rescaling the data to the same scale using standard scaling
    ## scaled feature = (feature - mean of feature)/standard deviation of feature
    ## feature gets rescaled between  0  and 1.
    from sklearn.preprocessing import  StandardScaler
    Sc = StandardScaler()
    #print(type(data_train))
    data_train_center = Sc.fit_transform(data_train)
    data_test_center = Sc.transform(data_test)


    #print(type(target_train))
    ## have to convert to data frame because Standard scaling
    ## cannot be done to a pandas series object
    target_train_center = Sc.fit_transform(pd.DataFrame(target_train))
    target_test_center = Sc.transform(pd.DataFrame(target_test))

    return data, target, data_train_center, data_test_center, target_train_center, target_test_center

def data_split_classification(df, target_column):#,transform=None):
    target = df[target_column]
    data = df.drop([target_column], axis=1)

    # creating test and train datasets
    from sklearn.model_selection import train_test_split
    data_train, data_test, target_train, target_test = train_test_split(data, target,
                                                                        test_size=0.33,
                                                                        random_state=42)
    print('training data shape : ',data_train.shape)
    print('training target shape : ',target_train.shape)
    print('test data shape : ',data_test.shape)
    print('test data shape : ',target_test.shape)
    print('-x-' * 50)
    print()

    # 5. rescaling the data to the same scale using standard scaling
    ## scaled feature = (feature - mean of feature)/standard deviation of feature
    ## feature gets rescaled between  0  and 1.
    from sklearn.preprocessing import  StandardScaler
    Sc = StandardScaler()
    #print(type(data_train))
    data_train_center = Sc.fit_transform(data_train)
    data_test_center = Sc.transform(data_test)

    return data, target, data_train_center, data_test_center, target_train, target_test

