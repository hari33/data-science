import os.path
import pandas as pd
import pandas_profiling
def read(Path, delimiter=',', sheet_name=None, header=None):
    ext = os.path.splitext(Path)[1]
    if ext == '.csv' or ext == '.txt' or ext == '.tsv':
        # if sep == ',':
        #     df = pd.read_csv(Path, sep=',', skipinitialspace=True)
        # elif sep == ' ':
        #     df = pd.read_csv(Path, sep=' ', skipinitialspace=True)
        df = pd.read_csv(Path, delimiter=delimiter, header=header,skipinitialspace=True, low_memory=False)

    elif ext == '.xlsx' or ext ==  '.xls':
        df = pd.read_excel(Path,sheet_name=sheet_name,header=header )

    print(df.head(5))

    df = df.apply(lambda col: pd.to_datetime(col, errors='ignore')
                  if col.dtypes == object else col, axis=0)


    # profile = pandas_profiling.ProfileReport(df)
    # profile.to_file(output_file='./report.html')

    print(df.columns)
    print(df.shape)
    print(df.describe(include='all').transpose)
    print(df.info(null_counts=True, verbose=True))
    c = [c for c in df.columns if len(set(df[c])) == 1]
    if len(c) != 0:
        df.drop(c, axis=1, inplace=True)

    return df

df =  read('/home/hariactyv/Machine_Learning/PDS_master/data/custdata.tsv', '\t',header=0)
# df = read('/home/hariactyv/Machine_Learning/PDS_master/CH_04/train.csv', delimiter=',', header=0)

import pandas as pd
from pymongo import MongoClient


def _connect_mongo(host, port, username, password, db):
    """ A util for making a connection to mongo """

    if username and password:
        mongo_uri = 'mongodb://%s:%s@%s:%s/%s' % (username, password, host, port, db)
        conn = MongoClient(mongo_uri)
    else:
        conn = MongoClient(host, port)


    return conn[db]


def read_mongo(db, collection, query={}, host='localhost', port=27017, username=None, password=None, no_id=True):
    """ Read from Mongo and Store into DataFrame """

    # Connect to MongoDB
    db = _connect_mongo(host=host, port=port, username=username, password=password, db=db)

    # Make a query to the specific DB and Collection
    cursor = db[collection].find(query)

    # Expand the cursor and construct the DataFrame
    df =  pd.DataFrame(list(cursor))

    # Delete the _id
    if no_id:
        del df['_id']

    print(df.head(5))

    df = df.apply(lambda col: pd.to_datetime(col, errors='ignore')
                  if col.dtypes == object else col, axis=0)

    # profile = pandas_profiling.ProfileReport(df)
    # profile.to_file(output_file='./report.html')

    print(df.columns)
    print(df.shape)
    print(df.describe(include='all').transpose)
    print(df.info(null_counts=True, verbose=True))
    c = [c for c in df.columns if len(set(df[c])) == 1]
    if len(c) != 0:
        df.drop(c, axis=1, inplace=True)

    return df

'''
## "mongodb://mittalHimanshu:qwerty1234@ds155596.mlab.com:55596/demo_database"

df =  read_mongo(db='demo_database',collection='bills', host='ds155596.mlab.com', port='55596',
                 username='mittalHimanshu', password='qwerty1234')
print(df.head(5))
df.to_csv('bills.csv', index=False)
'''



def read_postgresql(db, query=None, host='localhost', port='5432', username=None, password=None):
    """ Read from Postgres and Store into DataFrame """
    postgresql_uri = 'postgresql://%s:%s@%s:%s/%s' % (username, password, host, port, db)

    from sqlalchemy import create_engine
    engine = create_engine(postgresql_uri)

    df = pd.read_sql_query(query,con=engine)
    print(df.head(5))
    df = df.apply(lambda col: pd.to_datetime(col, errors='ignore')
                  if col.dtypes == object else col, axis=0)

    # profile = pandas_profiling.ProfileReport(df)
    # profile.to_file(output_file='./report.html')
    print(df.columns)
    print(df.shape)
    print(df.describe(include='all').transpose)
    print(df.info(null_counts=True, verbose=True))
    c = [c for c in df.columns if len(set(df[c])) == 1]
    if len(c) != 0:
        df.drop(c, axis=1, inplace=True)

    return df
'''
## engine = create_engine('postgresql://hari:hari@localhost:5432/hari')
query = 'SELECT * FROM hus LIMIT 1000'
df = read_postgresql(db='hari', query=query, username='hari', password='hari')
'''