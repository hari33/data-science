import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from plotnine import *

def linear(data_train_center, data_test_center, target_train_center, target_test_center, target, data):
    ## visualizing  distribution of target data
    import seaborn as sns
    sns.distplot(target, rug=True, rug_kws={"color": "g"},
                 kde_kws={"color": "r", "lw": 3, "label": "KDE"},
                 hist_kws={"histtype": "step", "linewidth": 3,
                           "alpha": 1, "color": "g"})

    plt.title("Distribition of Target")
    plt.show()

    ## feature selection
    ## Notice the code below has .95 for the number of components parameter.
    ## It means that scikit-learn choose the minimum number of principal components
    ## such that 95% of the variance is retained.

    from  sklearn.decomposition import PCA

    ## Make an instance of the Model
    pca = PCA(.95)

    ##  Note: you are fitting PCA on the training set only.
    model = pca.fit(data_train_center)

    ## Apply the mapping (transform) to both the training set and the test set.
    data_train_center= model.transform(data_train_center)
    data_test_center = model.transform(data_test_center)
    print('pca explained_variance_ratio_')
    print(model.explained_variance_ratio_)

    # number of components
    n_pcs= model.components_.shape[0]

    # get the index of the most important feature on EACH component i.e. largest absolute value
    # using LIST COMPREHENSION HERE
    most_important = [np.abs(model.components_[i]).argmax() for i in range(n_pcs)]

    initial_feature_names = data.columns

    # get the names
    most_important_names = [initial_feature_names[most_important[i]] for i in range(n_pcs)]

    # using LIST COMPREHENSION HERE AGAIN
    dic = {'PC{}'.format(i+1): most_important_names[i] for i in range(n_pcs)}

    # build the dataframe
    df = pd.DataFrame(sorted(dic.items()))
    print(df)

    ## implementing linear regression
    from sklearn.linear_model import LinearRegression
    linreg = LinearRegression()
    linreg.fit(data_train_center,target_train_center)

    ## predicting the target for input of data_test_center
    target_pred = linreg.predict(data_test_center)
    print(type(target_test_center))
    print(type(target_pred))

    ## compare  predicted and actual targets and visualize
    AP = pd.DataFrame({'Actual': target_test_center.flatten(),
                   'Predicted': target_pred.flatten()})
    print(AP)

    df1 = AP.head(25)

    df1.plot(kind='bar',figsize=(10,8))
    plt.grid(which='major', linestyle='-', linewidth='0.5', color='green')
    plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
    plt.show()

    ## viewing the intercept and coefficients
    print('intercept : ',linreg.intercept_)
    data = pd.DataFrame(data_train_center)
    coeff_df = pd.DataFrame(linreg.coef_.reshape(-1,1), data.columns, columns=['Coefficient'])
    print(coeff_df)
    print('-x-' * 50)

    # viewing the  the different measures of error
    from sklearn import metrics
    print('Mean Absolute Error:', metrics.mean_absolute_error(target_test_center, target_pred))
    print('Mean Squared Error:', metrics.mean_squared_error(target_test_center, target_pred))
    print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(target_test_center, target_pred)))



    #  Explained variance score: 1 is perfect prediction
    from sklearn.metrics import r2_score
    print('Variance score: %.2f' % r2_score(target_test_center, target_pred))


    ## extras

    fig = (ggplot(AP, aes(x= 'Predicted',y= 'Actual')) +
           geom_point(color='red', alpha = 0.5) +
           geom_smooth(method='lm', color='green') +
            labs(title='Scatter and Regression fit of Actual vs Predicted',
                 x='Actual',
                 y='Predicted'))

    print(fig)

    ## to get statistical result like R
    import statsmodels.api as sm
    model = sm.OLS(target_train_center, data_train_center).fit()
    print('Statistical resultS like R')
    print(model.summary())
    print(model.summary2())